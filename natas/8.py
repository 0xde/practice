import binascii, requests, base64, re

url = 'http://natas8.natas.labs.overthewire.org'
user = ('natas8', 'DBfUBfqQG69KvJvJ1iAbMoIpwSNQ9bWe')

encoded_secret = binascii.unhexlify('3d3d516343746d4d6d6c315669563362')

secret = base64.b64decode(encoded_secret[::-1]).decode()
submit = 'Submit+Query'

payload = {'secret': secret, 'submit': submit}

response = requests.post(url, auth=user, data=payload)

result = re.search(r'(?<=natas9 is )([A-Z])\w+', response.text)

print(result.group())
