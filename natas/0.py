"""
Grab the password out of a HTML comment
"""
from requests_html import HTMLSession

session = HTMLSession()

response = session.get("http://natas0.natas.labs.overthewire.org", auth=('natas0', 'natas0'))

result = response.html.search("<!--The password for natas1 is {} -->")[0]

print(result)
